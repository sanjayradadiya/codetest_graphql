import { getUserId, IContext } from "../../utils";

export const post = {
  async createDraft(parent, { title, content }, ctx: IContext, info) {
    const userId = getUserId(ctx);
    return ctx.prisma.createPost({
      author: {
        connect: { id: userId },
      },
      content,
      title,
    });
  },

  async publish(parent, { id }, ctx: IContext, info) {
    const userId = getUserId(ctx);
    const postExists = await ctx.prisma.$exists.post({
      author: { id: userId },
      id,
    });
    if (!postExists) {
      throw new Error(`Post not found or you're not the author`);
    }

    return ctx.prisma.updatePost({
      data: { published: true },
      where: { id },
    });
  },

  async deletePost(parent, { id }, ctx: IContext, info) {
    const userId = getUserId(ctx);
    const postExists = await ctx.prisma.$exists.post({
      author: { id: userId },
      id,
    });
    if (!postExists) {
      throw new Error(`Post not found or you're not the author`);
    }

    return ctx.prisma.deletePost({ id });
  },
};
